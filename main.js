let arr = ['hello', 'world', 23, '23', null, 15, true];

function filterBy(arr, type) {
    return arr.filter(function (value) {
        return typeof value === type
    })
}

console.log(filterBy(arr, 'boolean'));

